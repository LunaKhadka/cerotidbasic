package ProgressivePages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class HomePage {
	//Create an instance/object of WebDriver
			public static WebDriver driver;
			
			//constructor to create a object of this class
			public HomePage(WebDriver driver) {
				
				this.driver = driver;
				
			//Initialize the Web elements from this class
			PageFactory.initElements(driver,this);
			}
			
			
			//Locate all Elements that we are going to use from page
			//Annotation
			
		    @FindBy(xpath="//*[@id='main']/div[1]/div/div/div[2]/div[1]/ul/li[1]/a/p")
			WebElement auto;
		    
		    //Methods that utilize web elements from this page	
			public void autoQuote() {
	
			auto.click();
					
			}
			
			public boolean isPageOpened() {
				
			//assert true/false if page is opened (return turn if the title contains
				return driver.getTitle().contains("Quote Auto Insurance, Home-Auto Bundles, & More | Progreesive");
				
			}
			
			public void chooseAutoInsuracne() {
				// TODO Auto-generated method stub
				
			}

			



}
