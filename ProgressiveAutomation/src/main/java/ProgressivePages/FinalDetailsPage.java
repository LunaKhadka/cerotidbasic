package ProgressivePages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;

public class FinalDetailsPage {
	//Create an instance/object of WebDriver
	public static WebDriver driver;
	
	//constructor to create a object of this class
	public FinalDetailsPage(WebDriver driver) {
		
		this.driver = driver;
		
	//Initialize the Web elements from this class
	PageFactory.initElements(driver,this);
 }
	
	
			//Locate all Elements that we are going to use from page
			//Annotation
				
		    //Select Driver's Insurance History
			@FindBy(xpath="//input[@id=\'FinalDetailsEdit_embedded_questions_list_InsuranceToday_N\']")
			WebElement selectRecentInsurance;
			
			 //Select Driver's Insurance PasgtHistory
			@FindBy(xpath="//input[@id=\'FinalDetailsEdit_embedded_questions_list_InsuranceLastMonth_N\']")
			WebElement selectPastInsurance;
			
			//Select Driver's Insurance PasgtHistory
			@FindBy(xpath="//input[@id=\'FinalDetailsEdit_embedded_questions_list_OtherPolicies_N\']")
			WebElement selectProgressiveInsurance;
			
			@FindBy(xpath="//input[@id='FinalDetailsEdit_embedded_questions_list_PrimaryEmailAddress']")
			WebElement EmailAddress;
			
			@FindBy(xpath="//select[@id=\'FinalDetailsEdit_embedded_questions_list_TotalResidents\']")
			WebElement selectResidentInformation;
			
			@FindBy(xpath="//select[@id=\'FinalDetailsEdit_embedded_questions_list_CurrentResidence\']")
			WebElement selectCurrentResidenceTime;
			
			@FindBy(xpath="//select[@id=\'FinalDetailsEdit_embedded_questions_list_TotalPipClaimsCount\']")
			WebElement selectInjury;
			
			//Click Continue Button
			@FindBy(xpath="//button[contains(text(),'Continue')]")
			WebElement continueButton;
			
			//SnapshotEnrollment40Edit_embedded_questions_list_SnapshotPolicyEnrollment_N
			@FindBy(xpath="//input[@id=\'SnapshotEnrollment40Edit_embedded_questions_list_SnapshotPolicyEnrollment_N\']")
			WebElement selectPolicyEnrollment;
			
			//Click Continue Button
			@FindBy(xpath="//button[contains(text(),'Continue')]")
			WebElement continueBtn;
			
			//click no button
			@FindBy(xpath="//button[contains(text(),'No thanks, just auto')]")
			WebElement noBtn;

			
			
			

			//Methods that utilize webelements from this page
			public void selectInsuranceHistory() {
				
				selectRecentInsurance.click();
			}
			
          public void selectPastInsuranceHistory() {
				
        	  selectPastInsurance.click();
			}
          
          public void selectProgressiveInsurancePolicy() {
				
        	  selectProgressiveInsurance.click();
			}
          
          public void enterEmailAddress(String emailAddress) {
        	  EmailAddress.sendKeys(emailAddress);
          }
          
          public void selectResidentInformation(String resident) {
  			WebElement element = selectResidentInformation;
  			Select chooseResidentinfo = new Select(element);
  			
  			chooseResidentinfo.selectByVisibleText(resident);
          }
          
          public void selectCurrentResidenceTime(String CurrentResidence) {
    			WebElement element = selectCurrentResidenceTime;
    			Select chooseCurrentResidence = new Select(element);
    			
    			chooseCurrentResidence.selectByVisibleText(CurrentResidence);
            }
          
          public void selectInjury(String injury) {
  			WebElement element = selectInjury;
  			Select chooseInjury = new Select(element);
  			
  			chooseInjury.selectByVisibleText(injury);
          }
          
          public void clickContinueButton() {
        	  continueButton.click();
        		
        	}
          
          public void selectPolicyInformation() {
  			
        	  selectPolicyEnrollment.click();
  			
  			
  		}
          public void clickContinueBtn() {
        	  continueBtn.click();
        		
        	}
          public void clickNoBtn() {
        	  noBtn.click();
        		
        	}

  		




          
          




}