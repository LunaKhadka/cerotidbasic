package ProgressivePages;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class ClientInformationPage {
	//Create an instance/object of WebDriver
	public static WebDriver driver;
	
	//constructor to create a object of this class
	
	public ClientInformationPage(WebDriver driver) {
		
		this.driver = driver;
		
		//Initialize the Web elements from this class
		
		PageFactory.initElements(driver,this);
	}
	
	
	//Locate all Elements that we are going to use from page
	//Annotation
		
	//FirstName
	@FindBy(xpath="//input[@id='NameAndAddressEdit_embedded_questions_list_FirstName']")
	WebElement FirstName;
	
	//LastName
	@FindBy(xpath="//input[@id='NameAndAddressEdit_embedded_questions_list_LastName']")
	WebElement LastName;
		
	//DateOfBirth
	@FindBy(xpath="//input[@id='NameAndAddressEdit_embedded_questions_list_DateOfBirth']")
	WebElement DateOfBirth;
	
	//Address
	@FindBy(xpath="//question[@data-automation-id='mailingAddress']")
	WebElement StreetNameQuestion;
	
	//StreetName
	@FindBy(xpath="//input[@id='NameAndAddressEdit_embedded_questions_list_MailingAddress']")
	WebElement StreetName;
	
	//Apartment number
	@FindBy(xpath="//input[@id='NameAndAddressEdit_embedded_questions_list_ApartmentUnit']")
	WebElement AptNo;
	
	//City
	@FindBy(xpath="//input[@id='NameAndAddressEdit_embedded_questions_list_City']")
	WebElement City;
	
	//Start a Quote
	@FindBy(xpath="//forward-navigation/loading-button/button")
	WebElement startMyQuoteButton;



	public void streetNameEnter() {
		WebDriverWait Wait = new WebDriverWait(driver,10000);
		Wait.until(ExpectedConditions.visibilityOf(StreetName));
		
	}
	
	//Methods that utilize WebElement from this page
	
	public void NameandBirthdateEnter(String Firstname, String Lastname, String DOB) {
		
		FirstName.sendKeys(Firstname);
		LastName.sendKeys(Lastname);
		DateOfBirth.sendKeys(DOB);
		
	}
	
	
	
	
	public void StreetMailingAddress(String streetName, String apt, String city) {
	     StreetNameQuestion.click();
          StreetName.click();
          streetNameEnter();
	     StreetName.sendKeys(streetName);
//		 JavascriptExecutor jse = (JavascriptExecutor)driver;
//		 jse.executeScript("arguments[0].value='"+streetName+"';", StreetName);
		 	
		 
		 
		 AptNo.sendKeys(apt);
//		 City.sendKeys(city);
		
	}
	
	public void  startMyQuoteButton() {
		 startMyQuoteButton.click();
	}
		 
	
	

}
