package ProgressivePages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class EnterZipCode {

	public static WebDriver driver;
	//constructor to create a object of this class
	
	public EnterZipCode(WebDriver driver) {
			this.driver = driver;
		
		//Initialize the Web elements from this class
		PageFactory.initElements(driver,this);
	}
	
	// enter Zipcode
    @FindBy(xpath="//input[@id='zipCode_overlay']")
		WebElement  homePageZipField;
    
    

     @FindBy(xpath="//input[@id='qsButton_overlay']")
     WebElement button;
     

 	//Methods that utilize webelements from this page
	 public void zipcodeEnter(String zipCode) {
	
	homePageZipField.sendKeys(zipCode);
	
    }
	 
	 
     public void getAQuote() {
		button.click();
		// TODO Auto-generated method stub
		
	}
 	
}