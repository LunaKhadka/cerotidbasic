package ProgressivePages;

import org.openqa.selenium.WebDriver;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
public class VehicleInformationPage {
	
	
	//Create an instance/object of WebDriver
	public static WebDriver driver;
	
	//constructor to create a object of this class
	public VehicleInformationPage(WebDriver driver) {
		
		this.driver = driver;
		
	//Initialize the Web elements from this class
	PageFactory.initElements(driver,this);
	
	}
	//Locate all Elements that we are going to use from page
	//Annotation
		
    //Select vehicle year
	@FindBy(xpath="//li[contains(text(),'2019')]")
	WebElement selectVehicleYear;
	
	//select vehicle make field
	@FindBy(xpath="//li[contains(text(),'Audi')]")
	WebElement selectVehicleMake;
	
	
	//select vehicle Model
	@FindBy(xpath="//li[contains(text(),'A4')]")
	WebElement selectVehicleModel;
	
	// Select Primary use
	@FindBy(xpath="//select[@id='VehiclesNew_embedded_questions_list_VehicleUse'] ")
	WebElement selectPrimaryUse;
	

	// Select Own or lease
	@FindBy(xpath="//select[@id=\'VehiclesNew_embedded_questions_list_OwnOrLease']")
	WebElement selectOwnOrLease;
	
	//Select length of ownership
	@FindBy(xpath="//select[@id=\'VehiclesNew_embedded_questions_list_LengthOfOwnership']")
	WebElement selectLengthOfOwnership;
	
	
	//Click Done Button
	@FindBy(xpath="//button[contains(text(),'Done')]")
	WebElement selectDoneBtn;
	
	//Click Continue Button
	@FindBy(xpath="//button[contains(text(),'Continue')]")
	WebElement continueBtn;
	

	//Methods that utilize Web Elements from this page
	public void enterVehicleYear() {
		selectVehicleYear.click();
		
	}
	
	public void enterVehicleMake() {
		selectVehicleMake.click();
		
	}
	
	public void enterVehicleModel() {
		selectVehicleModel.click();
		
	}
	
	public void waitElement(WebElement element) {
		WebDriverWait Wait = new WebDriverWait(driver,5);
		Wait.until(ExpectedConditions.visibilityOf(element));
		
	}
	
	public void selectPrimaryUse(String vehicleUse) {
		WebElement element = selectPrimaryUse;
		Select choosePrimaryUse = new Select(element);
		
		choosePrimaryUse.selectByVisibleText(vehicleUse);

		
	}
	
	public void selectOwnOrLease(String leaseOrOwn){
		WebElement element = selectOwnOrLease;
		Select chooseOwnOrLease = new Select(element);
		
		chooseOwnOrLease.selectByVisibleText(leaseOrOwn);
		
		
	}
	
	public void selectLengthOfOwnership(String lengthOfOwnership){
		WebElement element = selectLengthOfOwnership;
		Select chooseLengthOfOwnership = new Select(element);
		
		chooseLengthOfOwnership.selectByVisibleText(lengthOfOwnership);
		
		
	}
	
	
	public void clickDoneBtn() {
		selectDoneBtn.click();
		
		
	}
	
	public void clickContinueBtn() {
		continueBtn.click();
		
	}

	
	

	
	
	


	


}
