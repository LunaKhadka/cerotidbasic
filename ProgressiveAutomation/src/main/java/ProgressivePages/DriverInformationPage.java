package ProgressivePages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;

public class DriverInformationPage {
	//Create an instance/object of WebDriver
		public static WebDriver driver;
		
		//constructor to create a object of this class
		public DriverInformationPage(WebDriver driver) {
			
			this.driver = driver;
			
		//Initialize the Web elements from this class
		PageFactory.initElements(driver,this);
		
		}
		
		//Locate all Elements that we are going to use from page
		//Annotation
			
	    //Select Driver's Gender
		@FindBy(xpath="//input[@id=\'DriversAddPniDetails_embedded_questions_list_Gender_F\']")
		WebElement selectGender;
		
		//Select Marital Status
		@FindBy(xpath="//select[@id=\'DriversAddPniDetails_embedded_questions_list_MaritalStatus\']")
		WebElement selectMaritalStatus;
		
		//Select Level Of Education
		@FindBy(xpath="//select[@id=\'DriversAddPniDetails_embedded_questions_list_HighestLevelOfEducation\']")
		WebElement selectLevelOfEducation;
		
		//Select Employment Status
		@FindBy(xpath="//select[@id=\'DriversAddPniDetails_embedded_questions_list_EmploymentStatus\']")
		WebElement selectEmploymentStatus;
		
		//Enter Social Security Number
		//@FindBy(xpath="//*[@id=\'DriversAddPniDetails_embedded_questions_list_SocialSecurityNumber\']")
		//WebElement enterSocialSecurityNumber;
		
		//Select Residence
		@FindBy(xpath="//select[@id=\'DriversAddPniDetails_embedded_questions_list_PrimaryResidence\']")
		WebElement selectPrimaryResidence;
		
		//Select MovedPrior
		@FindBy(xpath="//select[@id=\'DriversAddPniDetails_embedded_questions_list_HasPriorAddress\']")
		WebElement selectMovedPrior;
		
		//select Licence Year
		@FindBy(xpath="//select[@id=\'DriversAddPniDetails_embedded_questions_list_DriverYearsLicensed\']")
		WebElement selectLicenceYear;
		
		//selectDamages
		@FindBy(xpath="//input[@id=\'DriversAddPniDetails_embedded_questions_list_HasAccidentsOrClaims_N\']")
		WebElement selectDamages;
		
		//selectViolation
		@FindBy(xpath="//input[@id=\'DriversAddPniDetails_embedded_questions_list_HasTicketsOrViolations_N\']")
		WebElement selectViolation;
		
		//Click Continue Button
		@FindBy(xpath="//button[contains(text(),'Continue')]")
		WebElement continueBtn;
		
		//Click Continue Button
		@FindBy(xpath="//button[contains(text(),'Continue')]")
		WebElement continueButton;
				

		//Methods that utilize web elements from this page
		public void selectFemaleGender() {
			
			selectGender.click();
			
			
		}
		
		public void selectMaritalStatus(String single){
			WebElement element = selectMaritalStatus;
			Select chooseMaritalStatus = new Select(element);
			
			chooseMaritalStatus.selectByVisibleText(single);
			
			
		}
		
		public void selectLevelOfEducation(String education) {
			WebElement element = selectLevelOfEducation;
			Select chooseEducation = new Select(element);
			
			chooseEducation.selectByVisibleText(education);

			
			
		}
		public void selectEmploymentStatus(String employmentStatus) {
			WebElement element = selectEmploymentStatus;
			Select chooseEducation = new Select(element);
			
			chooseEducation.selectByVisibleText(employmentStatus);

		}
		
		public void selectPrimaryResidence(String primaryResidence) {
			WebElement element = selectPrimaryResidence;
			Select chooseEducation = new Select(element);
			
			chooseEducation.selectByVisibleText(primaryResidence);
		}
		
		public void selectMovedPrior(String MovedPrior) {
			WebElement element = selectMovedPrior;
			Select chooseEducation = new Select(element);
			
			chooseEducation.selectByVisibleText(MovedPrior);
		}
		
		public void selectLicenceYear(String LicenceYear) {
			WebElement element = selectLicenceYear;
			Select chooseEducation = new Select(element);
			
			chooseEducation.selectByVisibleText(LicenceYear);
		}
		
      public void selectNoDamages() {
			
    	  selectDamages.click();
			
			
		}
      
      public void selectVoilations() {
    	  
    	  selectViolation.click();
      }
      
      public void clickContinueBtn() {
  		continueBtn.click();
  		
  	}
      public void clickContinueButton() {
    	  continueButton.click();
    		
    	}



		

		






}
