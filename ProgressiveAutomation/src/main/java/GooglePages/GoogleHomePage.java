package GooglePages;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class GoogleHomePage {
	
	public WebDriver driver;
	
	//Create Webdriver object
	WebDriver fireDriver;
	
	
	public GoogleHomePage(WebDriver driver) {
		
		this.driver = driver;
		//Initialize the WebElements
		PageFactory.initElements(driver, this);
		
	}
	
	@FindBy(xpath="//input[@title='Search']")
	WebElement searchField;
	
	@FindBy(xpath="(//input[@name='btnK'])[2]")
	WebElement searchBtn;
	
	public void performSearch(String search) {
		searchField.click();
		searchField.sendKeys(search);
		searchBtn.sendKeys(Keys.RETURN);
		
		
	}
	
	
	

	

}
