package ProgressiveUtils;

import java.io.File;

import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;

import com.google.common.io.Files;

public class ScreenShotUtility {
	//Take a screenshot from the UI and store in system explorer

			public static void takeSnapShot(WebDriver driver,String screenShotName ) {
			try {
				//Creating a file object to take screenshots
				File src =((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
				//Store new File
				Files.copy(src,new File(".\\ScreenShots\\"+ screenShotName +".Jpeg" ));
			} catch (Exception e) {
				// TODO: handle exception
			}
			
		}


}
