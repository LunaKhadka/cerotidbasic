package ProgressivePagesTest;

import java.util.concurrent.TimeUnit;

import org.apache.commons.exec.environment.EnvironmentUtils;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.AssertJUnit;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import ProgressiveUtils.ScreenShotUtility;
import ProgressivePages.VehicleInformationPage;

public class StartAutoQuoteTestNG {
  
  //global variable
   WebDriver driver;
   
   //private Object vechileInformation;
   public static String browserName = null;
  
   //Step1
  @BeforeTest
  public void setUpTest() {
	 
	  
	  //Set System Path
	  System.setProperty("webdriver.chrome.driver", ".\\libs\\chromedriver.exe");
	  
	  //Use desired driver
	   driver = new ChromeDriver();
		  
 }
	  
  //Step 2
	  @Test(priority =0)
		public void StartAutoQuote() throws InterruptedException {
							
				//Create an object of the home page to utilize the UI elements
				ProgressivePages.HomePage homePage = new ProgressivePages.HomePage(driver);
				ProgressivePages.EnterZipCode enterzipcode = new ProgressivePages.EnterZipCode(driver);
				ProgressivePages.ClientInformationPage clientinformation = new ProgressivePages.ClientInformationPage(driver);
				ProgressivePages.VehicleInformationPage vehicleinformation = new ProgressivePages.VehicleInformationPage(driver);
				ProgressivePages.DriverInformationPage driverinformation = new ProgressivePages.DriverInformationPage(driver);
				ProgressivePages.FinalDetailsPage finalinformation = new ProgressivePages.FinalDetailsPage(driver);

								
				//set system path
				driver.get("https://www.progressive.com/");
				
				//Maximize the windows
				driver.manage().window().maximize();
				
				//Deleting all the cookies
				driver.manage().deleteAllCookies();
				driver.manage().timeouts().implicitlyWait(10,TimeUnit.SECONDS);
				
				//validate if the HomePage is opened
				//AssertJUnit.assertTrue(homePage.isPageOpened());
				
				//Selecting auto
				homePage.autoQuote();
				
				
				//Enter zip Code in zip code page
				enterzipcode.zipcodeEnter("75038");
				
				//Take SceenShot here
				ScreenShotUtility.takeSnapShot(driver, "ZipCodePage");
				
				
				//Click on Get a Quote
				enterzipcode.getAQuote();
				
				//Provide Name and Date of Birth in the ClientInformation Page
				clientinformation.NameandBirthdateEnter("Luna", "Khadka", "11/20/2000");
				
				//Provide address 
				clientinformation.StreetMailingAddress("3703 W Dahes Dr","2701","Irving");
				
				//click on Start a quote
				clientinformation.startMyQuoteButton();
				
				//select vehicle year
				vehicleinformation.enterVehicleYear();
				
				//select vehicle model
				vehicleinformation.enterVehicleMake();
				
				//Select Vehicle Make
				vehicleinformation.enterVehicleModel();
			
				
				//Select Primary use
				vehicleinformation.selectPrimaryUse("Personal (to/from work or school, errands, pleasure)");
				Thread.sleep(3000);
				
				//select own or lease
				vehicleinformation.selectOwnOrLease("Own");
				
				//select length of ownership
				vehicleinformation.selectLengthOfOwnership("Less than 1 month");
				Thread.sleep(3000);
				
				//select done
				vehicleinformation.clickDoneBtn();
				Thread.sleep(3000);
				
				//select continue button
				vehicleinformation.clickContinueBtn();
				Thread.sleep(3000);
				
				//Next Page
				//Select gender
				driverinformation.selectFemaleGender();
				
				//Select Marital Status
				driverinformation.selectMaritalStatus("Single");
				Thread.sleep(3000);
				
				//Select Level Of Education
				driverinformation.selectLevelOfEducation("College degree");
				
				//select Employment Status
				driverinformation.selectEmploymentStatus("Student (full time)");
				Thread.sleep(3000);
				
				//selectResidence
				driverinformation.selectPrimaryResidence("Own home");
				
				driverinformation.selectMovedPrior("No");
				Thread.sleep(3000);
				
				//select Licence Year
				driverinformation.selectLicenceYear("3 years or more");
				Thread.sleep(3000);
				
//				selectNoDamages
				driverinformation.selectNoDamages();
				
				//selectVoilations
				driverinformation.selectVoilations();
				
				//Click on Continue Provide
				driverinformation.clickContinueBtn();
				Thread.sleep(3000);
				
				//Click on Continue Provide
				driverinformation.clickContinueButton();
				Thread.sleep(3000);
				
				//select Insurance History
				finalinformation.selectInsuranceHistory();
				Thread.sleep(3000);
				
				//select Past Insurance History
				finalinformation.selectPastInsuranceHistory();
				Thread.sleep(3000);
				
				//select Progressive Insurance Policy
				finalinformation.selectProgressiveInsurancePolicy();
				Thread.sleep(3000);
				
               //Enter email address in Final Information Page
				finalinformation.enterEmailAddress("LunaKhadka2@gmail.com");
				
				//select Resident Information
				finalinformation.selectResidentInformation("1");
				Thread.sleep(3000);
				
				//select Current Residence Time
				finalinformation.selectCurrentResidenceTime("Less than 1 year");
				Thread.sleep(3000);
				
				//select Injury
				finalinformation.selectInjury("0");
				Thread.sleep(3000);
				
				//Click on Continue Provide
				finalinformation.clickContinueButton();
				Thread.sleep(3000);
				
				//selectPolicyInformation
				finalinformation.selectPolicyInformation();
                Thread.sleep(3000);
				
                //click Continue Button
				finalinformation.clickContinueButton();
				Thread.sleep(3000);
				
				 //click Continue Button
				finalinformation.clickNoBtn();
				
				
	}
	  //Step 3
@AfterTest
	private void terminate() {
		//close the browser
		driver.close();
		//quit the browser
		driver.quit();
	}
				
	  
  
  
  
 }
