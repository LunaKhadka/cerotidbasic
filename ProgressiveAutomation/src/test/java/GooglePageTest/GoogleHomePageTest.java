package GooglePageTest;

import java.util.concurrent.TimeUnit;

import org.junit.Before;
import org.junit.BeforeClass;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import GooglePages.GoogleHomePage;

public class GoogleHomePageTest {
	
	WebDriver fireDriver;
	
	
	
	@BeforeTest
	public void BeforeTest() {
		
		
		//set system path
		System.setProperty("webdriver.gecko.driver",".\\libs\\geckodriver.exe");
		
		//create a  new firefox object
		fireDriver = new FirefoxDriver();
		
		
	}
	
	public void navigateToHomePage() {

		fireDriver.get("https://www.google.com/");
		
		fireDriver.manage().timeouts().implicitlyWait(10,TimeUnit.SECONDS);
		

	}
	@Test(priority = 0)
	public void performSearch() {
		try {
			navigateToHomePage();
			
			GoogleHomePage homePage = new GoogleHomePage(fireDriver);
			homePage.performSearch("What is testng");

		}catch(NoSuchElementException e) {
			e.printStackTrace();
			
		}catch(Exception nse) {
			nse.printStackTrace();
		}
		
		
		
	}
	//@Test(priority = 1)
	//public void doNothing() {
		//System.out.println("Hello");
	//}
	
	@AfterTest
	public void terminate() {
		fireDriver.close();
		
	}

	

}
